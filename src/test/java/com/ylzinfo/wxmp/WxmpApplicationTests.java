package com.ylzinfo.wxmp;


import com.ylzinfo.wxmp.service.impl.WeixinServiceImpl;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * 测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootApplication
@SpringBootTest(classes = {WeixinServiceImpl.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WxmpApplicationTests {


    @Autowired
    WeixinServiceImpl weixinService;
    private Logger logger = LoggerFactory.getLogger(WxmpApplicationTests.class);

    private static final String TEMPLATE_ID = "jeSZIWjQK9IvOKO6BYXbrPLKP7JZadzO5ywf11_nGbo"; // 模板消息id

    /**
     * 配置菜单
     * @throws WxErrorException
     */
    @Test
    public void initMenu() throws WxErrorException {
        WxMenu menu = new WxMenu();
        WxMenuButton button1 = new WxMenuButton();
        button1.setName("医保资讯");
        WxMenuButton button11 = new WxMenuButton();
        button11.setType(WxConsts.MenuButtonType.VIEW);
        button11.setName("医保动态");
        button11.setUrl("http://qh.ylzms.com:1180/qh-medical/wechat/#/Informationone");
        WxMenuButton button12 = new WxMenuButton();
        button12.setType(WxConsts.MenuButtonType.VIEW);
        button12.setName("医保政策");
        button12.setUrl("http://qh.ylzms.com:1180/qh-medical/wechat/#/Informationtwo");
        button1.getSubButtons().add(button11);
        button1.getSubButtons().add(button12);
        menu.getButtons().add(button1);
        WxMenuButton button2 = new WxMenuButton();
        button2.setType(WxConsts.MenuButtonType.VIEW);
        button2.setName("演示样例");
        button2.setUrl("http://226t6s5912.iok.la/");
        menu.getButtons().add(button2);

        WxMenuButton button3 = new WxMenuButton();
        button3.setName("疫情专区");
        WxMenuButton button31 = new WxMenuButton();
        button31.setType(WxConsts.MenuButtonType.VIEW);
        button31.setName("疫情详情");
        button31.setUrl("https://fuwu-test.nhsa.gov.cn/curing/#/Epidemic/Detail");
        WxMenuButton button32 = new WxMenuButton();
        button32.setType(WxConsts.MenuButtonType.VIEW);
        button32.setName("疫情政策");
        button32.setUrl("https://fuwu-test.nhsa.gov.cn/curing/#/Epidemic/Policy");
        button3.getSubButtons().add(button31);
        button3.getSubButtons().add(button32);
        menu.getButtons().add(button3);
        String menuid = this.weixinService.getMenuService().menuCreate(menu);
        logger.info("配置菜单成功");
        
    }

    /**
     * 发送模板消息
     * @throws WxErrorException
     */
    @Test
    public void pushTemplateMsg() {
        String openid="oUNoOv2LSOMtq7eoZDgqOxw085rw";
        String name="张三";
        String time="202006";
        String money="3000";
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(openid)
                // 模板id
                .templateId(TEMPLATE_ID)
                // 可附加详情页面，不需要可以去掉
//                .url("http://www.baidu.com")
                .build();

        //填充模板数据
        templateMessage.addData(new WxMpTemplateData("name", name, "#000000"))
                .addData(new WxMpTemplateData("time", time, "#000000"))
                .addData(new WxMpTemplateData("money", money, "#000000"));

        String msgId = null;
        try {
            msgId = weixinService.getTemplateMsgService().sendTemplateMsg(templateMessage);
        } catch (WxErrorException e) {
            logger.info("【发送模板消息出错】{}", e);
        }
    }

}
