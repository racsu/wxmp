package com.ylzinfo.wxmp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @className: IndexController
 * @description:
 * @author: suhuanxiong
 * @create: 2020-05-27 16:02
 **/
@Controller
public class IndexController {

    @RequestMapping({"/index", "/"})
    public String index(){
        return "index";
    }

}
