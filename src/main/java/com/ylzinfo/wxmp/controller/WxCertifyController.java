package com.ylzinfo.wxmp.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.ylzinfo.dto.impl.CommonResult;
import com.ylzinfo.wxmp.dto.WxCertifyRequestDTO;
import com.ylzinfo.wxmp.service.impl.WeixinServiceImpl;
import com.ylzinfo.wxmp.util.HttpUtil;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;

import okhttp3.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Encoder;


import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * @className: WxCertifyController
 * @description: 微信实名认证控制器
 * 实名认证的demo，为演示方便以及流程清晰，未写service层、dao层，实际使用需要遵守mvc三层架构规范或者各自项目的规范
 * @author: suhuanxiong
 * @create: 2020-05-27 16:45
 **/
@Controller
public class WxCertifyController {

    @Autowired
    WeixinServiceImpl weixinService;
    private Logger logger = LoggerFactory.getLogger(WxCertifyController.class);

    private static String GET_IDENTIFY_INFO_URL = "https://api.weixin.qq.com/cityservice/face/identify/getinfo?access_token=";
    private static String GET_IDENTIFY_IMG_URL = "https://api.weixin.qq.com/cityservice/face/identify/getimage?access_token=";


    /**
     * 实人认证页面
     * @return
     */
    @RequestMapping("wxCertify")
    public String wxCertify() {
        return "wxCertify";
    }

    /**
     * 获取js签名，可参考附录1-JS-SDK使用权限签名算法，地址：
     * https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#4
     * @param url
     * @return
     */
    @RequestMapping("/getWxJsapiSignature")
    @ResponseBody
    public CommonResult getWxJsapiSignature(String url) {
        WxJsapiSignature wxJsapiSignature = null;
        try {
            wxJsapiSignature = weixinService.createJsapiSignature(url);
        } catch (WxErrorException e) {
            logger.info("【微信jssdk授权签名出错】{}", e);
            return CommonResult.errorResult("微信jssdk授权签名出错");
        }
        CommonResult result = CommonResult.successResult("获取js签名成功");
        result.setBeanToData(wxJsapiSignature);
        return result;
    }

    /**
     * 更新账号实人状态
     * 二次核验请求结果&&保存实人认证图片
     *
     * @param wxCertifyRequestDTO
     * @return
     */
    @PostMapping("checkCertifyResult")
    @ResponseBody
    public CommonResult checkCertifyResult(WxCertifyRequestDTO wxCertifyRequestDTO) {

        wxCertifyRequestDTO.setId_card_number(wxCertifyRequestDTO.getId_card_number().toUpperCase());// 提升为大写，避免校验出错


        /**
         * 获取accessToken
         */
        String wxAccessToken = null;
        try {
            wxAccessToken = weixinService.getAccessToken();
        } catch (WxErrorException e) {
            logger.info("【获取微信接口accessToken出错】{}", e);
            return CommonResult.errorResult("获取微信接口accessToken出错");
        }
        logger.info("accessToken：{}", wxAccessToken);
        logger.info("verify_result：{}", wxCertifyRequestDTO.getVerify_result());
        /**
         * 二次核验实人认证结果
         */
        String verifyJson = JSONObject.toJSONString(wxCertifyRequestDTO);
        String jsonStr = HttpUtil.httpPost(GET_IDENTIFY_INFO_URL + wxAccessToken, verifyJson);
        JSONObject json = JSONObject.parseObject(jsonStr);
        Integer errcode = json.getInteger("errcode");
        String errmsg = json.getString("errmsg");
        Integer identify_ret = json.getInteger("identify_ret");
        String id_card_number_md5 = json.getString("id_card_number_md5");
        String name_utf8_md5 = json.getString("name_utf8_md5");
        if (errcode != null && errcode == 0 && identify_ret != null && identify_ret == 0) {
            String nameMd5 = DigestUtils.md5Hex(wxCertifyRequestDTO.getName());
            String idCardNumMd5 = DigestUtils.md5Hex(wxCertifyRequestDTO.getId_card_number());
            if (!nameMd5.equals(name_utf8_md5)) {
                return CommonResult.errorResult("姓名校验失败");
            }
            if (!idCardNumMd5.equals(id_card_number_md5)) {
                return CommonResult.errorResult("身份证校验失败");
            }

            // TODO: 业务处理，保存身份证，姓名，实名状态，verify_result


            /**
             * 如业务需要保存人脸识别照片，可以使用以下代码
             * 拉取人脸验证时的照片
             * 如需要拉取人脸照片的接口，申请人脸识别时需要特别声明需要开通获取照片接口
             * 如需要拉取人脸照片的接口，申请人脸识别时需要特别声明需要开通获取照片接口
             * 如需要拉取人脸照片的接口，申请人脸识别时需要特别声明需要开通获取照片接口
             */
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("applicationon/json");
            RequestBody body = RequestBody.create(mediaType, verifyJson);
            Request request = new Request.Builder()
                    .url(GET_IDENTIFY_IMG_URL + wxAccessToken)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                logger.info("【获取人脸照片时出错】{}", e);
//                return CommonResult.errorResult("获取人脸照片时出错");
            }

            //判断响应类型，如果响应是image/jpeg,则获取成功，其他情况获取失败
            String contentType = response.header("Content-Type", "application/json; encoding=utf-8");
            if ("image/jpeg".equals(contentType)) {
                byte[] bytes = new byte[0];
                try {
                    bytes = response.body().bytes();
                } catch (IOException e) {
                    logger.info("【人脸照片转换字节数组时出错】{}", e);
                    return CommonResult.errorResult("人脸照片转换字节数组时出错");
                }
                String base64Str = new BASE64Encoder().encode(bytes); // 人脸照片的base字符串
                logger.info("=================获取到的人脸照片结果============");
                logger.info(base64Str);
                logger.info("=================获取到的人脸照片结果============");
            } else {
                String res = null;
                try {
                    res = response.body().string();
                } catch (IOException e) {
                    logger.info("【获取人脸照片出错】{}", res);
//                    return CommonResult.errorResult("获取人脸照片出错");
                }
            }

            return CommonResult.successResult("实人认证成功");
        } else {

            logger.info("【微信实人认证核验验证结果出错】errmsg{}", errmsg);
            return CommonResult.successResult(errmsg);
        }
    }
}
