package com.ylzinfo.wxmp.controller;

import com.ylzinfo.dto.impl.CommonResult;
import com.ylzinfo.dto.impl.ResultConfig;
import com.ylzinfo.wxmp.service.impl.WeixinServiceImpl;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @className: OAuth2Controlle
 * @description:微信网页授权的demo，为演示方便以及流程清晰，未写service层、dao层，实际使用需要遵守mvc三层架构规范或者各自项目的规范
 * @author: suhuanxiong
 * @create: 2020-06-05 17:55
 **/
@Controller
public class OAuth2Controlle {

    @Autowired
    WeixinServiceImpl weixinService;
    private Logger logger = LoggerFactory.getLogger(OAuth2Controlle.class);

    /**
     * 微信网页授权demo页
     *
     * 网页授权这个说法可能有点高级？说简单点就是获取openid
     * @return
     */
    @RequestMapping("OAuth2")
    public String OAuth2() {
        return "OAuth2";
    }


    /**
     * 构造获取授权码code的链接
     * @param url
     * @param state
     * @return
     */
    @RequestMapping("/authorize")
    @ResponseBody
    public CommonResult authorize(String url, String state) {
        String redirectUrl = weixinService.oauth2buildAuthorizationUrl(url, WxConsts.OAuth2Scope.SNSAPI_BASE, state);
        logger.info("【微信网页授权】构造获取code链接,redirectURL={}", redirectUrl);
        Map resMap = new HashMap<>();
        resMap.put("redirectUrl", redirectUrl);
        return CommonResult.successResult(ResultConfig.MSG_SUCCESS, resMap);
    }

    /**
     * 解析授权码为openid
     * @param code
     * @return
     */
    @RequestMapping("/parseCode")
    @ResponseBody
    public CommonResult getOpenId(String code) {
        logger.info("【微信网页授权】code={}", code);
        WxMpOAuth2AccessToken wxMpOAuth2AccessToken;
        try {
            wxMpOAuth2AccessToken = weixinService.oauth2getAccessToken(code);
        } catch (WxErrorException e) {
            logger.error("【微信网页授权异常】{}", e);
            return CommonResult.errorResult(e.getError().getErrorMsg());
        }
        String openId = wxMpOAuth2AccessToken.getOpenId();
        logger.info("【微信网页授权解析openid成功】openId={}", openId);
        /**
         * 解析openid成功，建议再此处继续进行业务操作，不建议将openid返回至前端或者存到cookie
         */
        return CommonResult.successResult("解析openid成功");
    }
}
