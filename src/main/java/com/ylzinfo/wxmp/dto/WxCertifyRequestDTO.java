package com.ylzinfo.wxmp.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @className: WxCertifyRequestDTO
 * @description:
 * @author: suhuanxiong
 * @create: 2020-05-27 17:57
 **/

public class WxCertifyRequestDTO implements Serializable {

    private static final long serialVersionUID = -3372447672576508168L;

    private String verify_result;


    private String name;


    private String id_card_number;


    public String getVerify_result() {
        return verify_result;
    }

    public void setVerify_result(String verify_result) {
        this.verify_result = verify_result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId_card_number() {
        return id_card_number;
    }

    public void setId_card_number(String id_card_number) {
        this.id_card_number = id_card_number;
    }
}

