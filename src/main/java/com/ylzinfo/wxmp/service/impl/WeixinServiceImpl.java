package com.ylzinfo.wxmp.service.impl;


import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;

import me.chanjar.weixin.mp.api.WxMpUserService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.api.impl.WxMpUserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


/**
 * 功能描述:WxJava的加载服务，全项目需要调用SDK的地方，需要注入本service即可
 *
 * @author shx
 * @date 2019/11/1 9:20
 */
@Service
public class WeixinServiceImpl extends WxMpServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String APPID = "你的appid";
    private static final String APPSECRET = "你的appsecret";

    /**
     * 初始化参数
     */
    @PostConstruct
    public void init() {
        //使用redis缓存配置，需要注入JedisPool
//        final WxMpInRedisConfigStorage config = new WxMpInRedisConfigStorage(jedisPool);
        //演示使用本地缓存模式
        WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
        // 设置微信公众号的appid
        config.setAppId(APPID);
        // 设置微信公众号的appSecret
        config.setSecret(APPSECRET);
        super.setWxMpConfigStorage(config);
    }

    @Bean
    public WxMpUserService registrationBean(){
        WxMpUserService wxMpUserService = new WxMpUserServiceImpl(this);
        return wxMpUserService;
    }
}
